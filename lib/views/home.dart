import 'package:flutter/material.dart';
import 'package:kyc/repositories/book.dart';
import 'package:kyc/repositories/book_remote.dart';
import 'package:kyc/models/book.dart';


class MyHomePage extends StatelessWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;


  @override
  Widget build(BuildContext context) {
    BookRepository bookRepo = BookRepositoryRemote();
    Stream<List<Book>> books = bookRepo.getAll();

    return Scaffold(
        appBar: AppBar(
          title: Text(title),
        ),
        body: Container(
          child: StreamBuilder<List<Book>>(
            stream: books,
            initialData: const [],
            builder: (
              BuildContext context,
              AsyncSnapshot<List<Book>> bookList,
            ) {
              return ListView(
                children: bookList.data!
                    .map(
                      ((Book book) => Text(book.title)),
                    )
                    .toList(),
              );
            },
          ),
        ));
  }
}
