import 'package:flutter/material.dart';
import 'package:kyc/views/home.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "demo app",
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: '/',
      routes: {
        '/home': (context) => const MyHomePage(title: 'home'),
      },
      debugShowCheckedModeBanner: false,
      onGenerateRoute: (settings) {
        return MaterialPageRoute(
            builder: (BuildContext context) => const MyHomePage(title: 'home'));
      },
    );
  }
}
